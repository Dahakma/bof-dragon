import { defineConfig } from 'vite'
import eslint from "@rollup/plugin-eslint"


export default defineConfig({
	
	server: {
		open: '/index.html'
	},
	
	plugins: [
		{
			...eslint({
				fix: false,
				exclude:  [
					`src/**/*.css`,
				],
			}),
			enforce: 'pre',
			//apply: 'build',
		},
	]
})
